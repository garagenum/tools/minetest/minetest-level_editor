	-- Set unique global
level_editor = {}
level_editor.levels = {}

	-- First thing, call load/save functions
dofile(minetest.get_modpath("level_editor").."/src/save.lua")
level_editor.load()

minetest.register_on_shutdown(function()
	level_editor.save()
end)

	-- Call utils
dofile(minetest.get_modpath("level_editor").."/src/utils.lua")


	-- Settings in minetest.conf
minetest.settings:set_bool("enable_pvp",false)  -- can be change durig game
minetest.settings:set_bool("enable_damage",true) -- can be change durig game
-- minetest.settings:set("fixed_map_seed","legaragenumerique")  -- not useful because minetest.set_mapgen_setting handles it in mapping.lua
--minetest.settings:set("mg_name","v5")  -- is overriden by GUI
minetest.settings:set_bool("creative_mode",false)
minetest.settings:set_bool("projecting_dungeons",false)  -- don't know if it works
minetest.settings:write()


	-- Register privileges
minetest.register_privilege("level", {description = "Able to place levels and checkpoints", give_to_singleplayer= false})

	-- register existing levels in /levels
local register_levels = nil
register_levels = function()
	local level_dir = minetest.get_modpath("level_editor").."/levels/"
	local base_levels = minetest.get_dir_list(level_dir,false)  -- false returns only file names
	local textures_dir = minetest.get_modpath("level_editor").."/textures/"
	
	for i,v in ipairs(base_levels) do
		if v:match("(%.conf)$") then
			local level,_ = v:match("(.-)(%.conf)$")
			local leveldata = level_editor.getleveldata(level)
			
			local settings = Settings(level_dir..v)
			local conf = settings:to_table()
			
			if not conf.picture and level_editor.check_file(textures_dir..conf.name..".png") then
				conf.picture = conf.name..".png"
			elseif conf.picture and not level_editor.check_file(textures_dir..conf.picture..".png") 
				and level_editor.check_file(textures_dir..conf.name..".png") then
				conf.picture = conf.name..".png"
			else conf.picture = "default_level_access.png"
			end
			
			if conf.privs then
				local privs = {}
				local comma = conf.privs:match(",")
				if not comma then table.insert(privs,conf.privs) else
					for i in conf.privs:gmatch(".-,") do
						i = i:sub(1,#i-1)
						table.insert(privs,i)
					end
					local last = conf.privs:reverse()
					last = last:match(".-,")
					last = last:sub(1,#last-1)
					last = last:reverse()
					if last then table.insert(privs,last) end
				end
				conf.privs = privs
			end

			if conf.basepoint then
				conf.basepoint = minetest.string_to_pos(conf.basepoint)
			end

			if conf.startpoint then
				conf.startpoint = minetest.string_to_pos(conf.startpoint)
			end
			if conf.protection_start then
				conf.protection_start = minetest.string_to_pos(conf.protection_start)
			end		
			if conf.protection_end then
				conf.protection_end = minetest.string_to_pos(conf.protection_end)
			end	
			if conf.size then
			conf.size = minetest.string_to_pos(conf.size)
			end	
			conf.realstartpoint = {x=0,y=0,z=0}
							
			print(dump(conf))
			print("name "..type(conf["name"]).." "..conf.name)
			leveldata.base_def = conf
			level_editor.save()
		end
	end
end
register_levels()

	-- Calls selector.lua
dofile(minetest.get_modpath("level_editor").."/src/selector.lua")

	-- Calls checkpoints.lua
dofile(minetest.get_modpath("level_editor").."/src/checkpoints.lua")

	-- Calls mapping.lua
dofile(minetest.get_modpath("level_editor").."/src/protection.lua")

	-- Calls mapping.lua
dofile(minetest.get_modpath("level_editor").."/src/mapping.lua")

	-- Calls editor.lua
dofile(minetest.get_modpath("level_editor").."/src/editor.lua")

	-- Calls levels.lua
dofile(minetest.get_modpath("level_editor").."/src/levels.lua")

minetest.register_on_respawnplayer(function(player)
	local name = player:get_player_name()
	local pldata = level_editor.getplayerdata(name)
	if pldata.actual_level == "" then
		minetest.log("action","no previous level accessed by "..name.." on respawn")
		return
	else
		minetest.log("action","level accessed by "..name.." on respawn : "..pldata.actual_level)
		local pos = pldata.checkpoint[#pldata.checkpoint]
		local hp = pldata.registered_levels[pldata.actual_level].hp
		level_editor.go_checkpoint(name)
		if hp then player:set_hp(hp) end
		return true
	end
end)

minetest.register_on_newplayer(function(player)
	local name = player:get_player_name()
	local pldata = level_editor.getplayerdata(name)
	if pldata.actual_level == "" then
		level_editor.go_level(name,"home")
		return
	end
end)

	-- End of init
