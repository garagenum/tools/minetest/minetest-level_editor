-- preparing map 
minetest.set_mapgen_setting("seed","legaragenumerique", true)  --this works
--minetest.set_mapgen_setting("mgname","fractal",true)  -- mgname doesn't repalce gui setting
--minetest.set_mapgen_setting("flags","nodungeons,nocaves",true) -- this neither

-- minetest.line_of_sight(pos1, pos2)
-- minetest.line_of_sight(pos1, pos2)

function level_editor.get_schem(level)
	local filename	= minetest.get_modpath("level_editor").."/levels/"..level..".mts"
	local file		= io.open(filename, "r")
	if file == nil then 
		--io.close(file) 
		minetest.log("action","file not found : "..filename)
		return nil 
	else
		local s = file:read(12)
		local size = {
			x = s:byte(7)*256+s:byte(8),  
			y = s:byte(9)*256+s:byte(10),
			z = s:byte(11)*256+s:byte(12)	
			}
		io.close(file)
		return filename, size
	end
end

function level_editor.put_schem(name, level)
	local dir = minetest.get_modpath("level_editor").."/levels/"
	local pldata = level_editor.getplayerdata(name)
	local leveldata = level_editor.getleveldata(level)
	local ldef = pldata.registered_levels[level] --or leveldata.base_def
	local pos = ldef.basepoint
	local schem, size = level_editor.get_schem(level)
	if schem ~= nil then
		minetest.log("action","Schematic used : "..schem.." with dim : x = "..size.x.." , y = "..size.y.." , z = "..size.z)
		if minetest.place_schematic(pos, schem,"0",{},true) == nil then
			minetest.log("action","le niveau "..level.."n'a pas pu être généré pour "..plname)
			minetest.chat_send_player("bellinuxien","le niveau "..level.."n'a pas pu être généré pour "..plname)
			return false
		else minetest.log("action",level.." generated at "..minetest.pos_to_string(pos))
			return size
		end
	end
end	

--[[
local complete = false
local function set_base(sender,params)
	local level = params:match("%w+")
	local player = minetest.get_player_by_name(sender)
	local data = level_editor.player(sender)
	
	local pos = player:get_pos()
	local stringpos = minetest.pos_to_string(pos)
	local def = data.registered_levels[level]

	if def then
		for i,v in pairs(pos) do
			def.basepoint[i] = math.ceil(pos[i])
		end
		data.registered_levels[level] = def
		level_editor.save()
		minetest.chat_send_player(sender,"base of "..level.." defined at "..minetest.pos_to_string(pos))--. Check your position with F5"
		level_editor.put_schem(sender,level)
		if complete == false then
			minetest.chat_send_player(sender,"Now you should define new start point using /levelstart "..level)
		else
			arrpos = {x=0,y=0,z=0}
			for i,v in pairs(def.realstartpoint) do
				def.startpoint[i] = v - def.basepoint[i]
				def.startpoint[i] = math.ceil(def.startpoint[i])
			end 
			
			local file = minetest.get_modpath("level_editor").."/levels.lua"
			minetest.chat_send_player(sender,"Last thing for "..level.." : define startpoint = "..
				minetest.pos_to_string(def.startpoint)..
				"in "..file)
			data.registered_levels[level] = def
	--		__player_data[sender] = data
			level_editor.save()
			complete = false
		end 
	else
		minetest.chat_send_player(sender,"no level defined as "..level)
	end
end


minetest.register_chatcommand("levelbase", {
	params = "name of level",
	privs = "level",
	description = "define basepoint for level",
	func = function(sender,params)
		set_base(sender,params)
	end,
})	

local function set_start(sender,params)
	local level = params:match("%w+")
	local player = minetest.get_player_by_name(sender)
	local data = level_editor.player(sender)
	local pos = player:get_pos()
	
	local def = data.registered_levels[level]
	
	if def then
		for i,v in pairs(def.basepoint) do
			def.startpoint[i] = pos[i] - v
			def.startpoint[i] = math.ceil(def.startpoint[i])
			def.realstartpoint[i] = math.ceil(pos[i])
		end
		data.registered_levels[level] = def
		level_editor.save()
		minetest.log("action","new start pointdump start pos : "..minetest.pos_to_string(def.startpoint))
		minetest.chat_send_player(sender,"Real Start point for "..level.." has been defined at "..minetest.pos_to_string(def.realstartpoint))
		minetest.chat_send_player(sender,"Define startpoint at "..
			minetest.pos_to_string(def.startpoint)..
			" for level "..level.." in "..
			minetest.get_modpath("level_editor").."/levels.lua")
		complete = true
	end
end
					
					
minetest.register_chatcommand("levelstart", {
	params = "",
	privs = "level",
	description = "Envoyer un hud à un joueur (ou à tous les joueurs *)",
	func = function(sender,params,complete)
		set_start(sender,params,complete)
	end,
})	
--]]
