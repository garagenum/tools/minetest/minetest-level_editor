local storage = minetest.get_mod_storage()
local __player_data
local __level_data
local __protection_data

-- Table Save Load Functions
function level_editor.save()
	storage:set_string("player_data",minetest.serialize(__player_data))
	storage:set_string("level_data",minetest.serialize(__level_data))
	storage:set_string("protection_data",minetest.serialize(__protection_data))
	--..dump(minetest.deserialize(storage:get_string("level_data")).count_gen))
	-- To use json we can't mix Integer and key values
	--storage:set_string("protection_data", minetest.write_json(__protection_data))
end

function level_editor.load()
--	__protection_data = minetest.parse_json(storage:get_string("protection_data")) or {}
	__player_data = minetest.deserialize(storage:get_string("player_data")) or {}
	__level_data = minetest.deserialize(storage:get_string("level_data")) or {}
	__protection_data = minetest.deserialize(storage:get_string("protection_data")) or {}
end

function level_editor.getleveldata(level)
	assert(type(level) == "string")
	local data = __level_data[level] or {}
	data.base_def =  data.base_def or {}
	--data.base_def.basepoint = data.base_def.basepoint
	data.count_gen = data.count_gen or 0
	data.protected = data.protected or {}
	__level_data[level] = data
	--level_editor.save()
	return data
end

function level_editor.getprotectiondata()
--	assert(type(name) == "string")
	local data = __protection_data or {}
--	data.startpoint = data.startpoint or {}
--	data.endpoint = data.endpoint or {}
--	data.owner = data.owner or ""
	__protection_data = data
	--level_editor.save()
	return data
end

function level_editor.getplayerdata(name)
	assert(type(name) == "string")
	local data = __player_data[name] or {}
	
	data.name     = data.name or name
	data.checkpoint = data.checkpoint or {}
	data.actual_level = data.actual_level or ""
	data.registered_levels = data.registered_levels or {}
	data.hud_level_infos = data.hud_level_infos or {}
	__player_data[name] = data
	--level_editor.save()
	return data
end


function level_editor.player_or_nil(name)
	return __player_data[name]
end

function level_editor.enable(name)
	level_editor.player(name).disabled = nil
end

function level_editor.disable(name)
	level_editor.player(name).disabled = true
end

function level_editor.clear_player(name)
	__player_data[name] = {}
end
