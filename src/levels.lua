level_editor.register_level = function(name,level)
	local player = minetest.get_player_by_name(name)
	local pldata = level_editor.getplayerdata(name)
	local leveldata = level_editor.getleveldata(level)
	
	local count_gen = leveldata.count_gen +1
	
	local ldef = {}
	ldef = table.copy(leveldata.base_def)
	--[[
	for k,v in pairs(leveldata.base_def) do
		--ldef = leveldata.base_def
		if type(v) == "string" then
			ldef[k] = v
		elseif type(v) == "table" then
			ldef[k] = {}
			for c,r in pairs(v) do
				ldef[k][c] = r
			end
		end
	end
	--]]
	
	local _,size = level_editor.get_schem(level)
	ldef.size = size or ldef.size or {x=50,y=50,z=50}

	if ldef.mode and ldef.mode == "single" then
			ldef.basepoint.x = ldef.basepoint.x + ldef.size.x * (count_gen - 1)
		--	ldef.basepoint.y = ldef.basepoint.y + ldef.size.y * (count_gen - 1)
		--	ldef.basepoint.z = ldef.basepoint.z + ldef.size.z * (count_gen - 1)			
	end

	for i,v in pairs(ldef.basepoint) do
		ldef.realstartpoint[i] = ldef.basepoint[i] + ldef.startpoint[i]
	end	
	
	if ldef.protection and ldef.protection == "all" then
		ldef.protected = {}		
		ldef.protected.startpoint = {}
		ldef.protected.startpoint = table.copy(ldef.basepoint)
		--[[
		for k,v in pairs(ldef.basepoint) do
			ldef.protected.startpoint[k] = v
		end
		--]]
		ldef.protected.endpoint = {}
		for k,v in pairs(ldef.size) do
			ldef.protected.endpoint[k] = ldef.protected.startpoint[k] + v
		end
	end
	
	if ldef.protection_start and ldef.protection_end then
		ldef.protected = {}
		ldef.protected.startpoint = {}
		ldef.protected.endpoint = {}
		for k,v in pairs(ldef.basepoint) do
			ldef.protected.startpoint[k] = ldef.basepoint[k] + ldef.protection_start[k]
			ldef.protected.endpoint[k] = ldef.basepoint[k] + ldef.protection_end[k]
		end
	end		

	pldata.registered_levels[level] = ldef
	leveldata.count_gen = count_gen
	level_editor.save()
	
	--ldef = {}
	--ldef = pldata.registered_levels[level] 
	local start_prot = ldef.protected.startpoint
	local end_prot = ldef.protected.endpoint
	if	(ldef.mode and ldef.mode == "single") or count_gen == 1 then
		level_editor.put_schem(name,level)
		if ldef.protection or (ldef.protection_start and ldef.protection_end) then
			print(level.." will be protected from "..minetest.pos_to_string(start_prot))
			local area_name = level.."_"..start_prot.x
			if ldef.owned and ldef.owned =="true" then
				print("by "..name)
				level_editor.protect_area(start_prot ,end_prot,area_name,name)
			else level_editor.protect_area(start_prot ,end_prot,area_name)
				print("by no one")
			end
		end
	end
		
--[[
	if not (ldef.mode and ldef.mode == "single") and leveldata.count_gen > 1 then
	else
		level_editor.put_schem(name,level)	
	end	
--]]	
	if ldef.registercall then
		ldef.registercall()
	end
	minetest.log("action","player "..name.." registered new level :"..level)
end
	
level_editor.go_level = function(name, level)
	local player = minetest.get_player_by_name(name)
	
		--Terminate previous level
	local pldata = level_editor.getplayerdata(name)
	local oldlev = pldata.actual_level
	print("oldlev : "..oldlev)
	if oldlev ~= "" then
		local oldldef = pldata.registered_levels[oldlev]
		if oldldef.endcall then
			oldldef.endcall(player)
		end

		if oldldef.hp then
			player:set_hp(20)
		end
	end

		-- Save datas
	minetest.log("action", name.." has entered level "..level)
	pldata.actual_level = level
	level_editor.save()
	
		-- Register level if not yet
	local ldef = pldata.registered_levels[level]
	if not ldef then
		level_editor.register_level(name,level)
		pldata = level_editor.getplayerdata(name)
	end
		-- Enter in new level
	local nldef = pldata.registered_levels[level]
	if nldef.realstartpoint then 
	--[[
		local realstartpoint = {}
		for k,v in pairs(nldef.realstartpoint) do
			realstartpoint[k] = v
		end
	--]]
		level_editor.activate_checkpoint(name,nldef.realstartpoint)
		level_editor.go_checkpoint(name)
		pldata = level_editor.getplayerdata(name)
	end
	if level == "deplacements" then
		minetest.settings:set_bool("enable_pvp",true)
--minetest.settings:set_bool("enable_damage",false)
minetest.settings:write()
end
	if nldef.startcall then
		nldef.startcall()
	end
	
	if nldef.hp then 
		player:set_hp(nldef.hp)
	end
	
	local privs = {}
	if nldef.privs then
		for i,v in ipairs(nldef.privs) do
			privs[v] = true
		end
	end
	privs.interact = true
	privs.shout = true
	minetest.set_player_privs(name,privs)
	
	
	local physics = player:get_physics_override()
	if nldef.speed then
		physics.speed = nldef.speed
	else physics.speed = 1
	end
	if nldef.gravity then
		physics.gravity = nldef.gravity
	else physics.gravity = 1
	end
	if nldef.jump then
		physics.jump = nldef.jump
	else physics.jump = 1
	end
	player:set_physics_override(physics)

	if nldef.infos then
	end
end

minetest.register_chatcommand("goto", {
	params = "< enfant | * for allplayers> , <level>",
	privs = "level",
	description = "Envoyer un joueur (ou tous les joueurs *) directement à un niveau",
	func = function(sender,options)
			local dest,level = string.match(options,"([%S]+) ([%S]+)")
			local leveldata = level_editor.getleveldata(level)
		--	print("dump leveldata : "..dump(leveldata))
			local check_table = nil
			local len = 0
			local len_table = function(table)
			  for k,v in pairs(table) do
				len = len + 1
			  end
			  return len
			end
			print("base_def for "..level.." : "..dump(leveldata.base_def).." has len = "..len_table(leveldata.base_def))
			if len_table(leveldata.base_def) ~= 0 then
				print("condition ok")
				if dest == "*" then
					for _,player in ipairs(minetest.get_connected_players()) do
						local name = player:get_player_name()
						level_editor.go_level(name,leveldata.base_def.name)
					end
				elseif dest and level then
					local player = minetest.get_player_by_name(dest)
					if not player then
						minetest.chat_send_player(sender,"in goto cmd - no player with this name : "..dest)
						return
					else level_editor.go_level(dest,leveldata.base_def.name)
					end
				else minetest.chat_send_player(sender,"you have to define level name AND player name") 
				end
			else
				leveldata = nil
				if dest and level then
					minetest.chat_send_player(sender,"in goto cmd - no level with this name : "..level)
				else minetest.chat_send_player(sender,"you have to define player name AND level name")
				end
			end
			return
		end,
	})
