local fs_b1 = 1	


local mk_fs = nil
mk_fs = function(name,level,...)
	local leveldata = level_editor.getleveldata(level)
	local base_def = {}
	base_def = table.copy(leveldata.base_def)

	local fs_picture = base_def.picture or "info_courir.png"
	local stringpos
	if base_def.basepoint then
		stringpos = minetest.pos_to_string(base_def.basepoint)
	end
	local fs_basepoint = stringpos or "0,0,0"
	print("base_def at gui show : "..dump(base_def))	
	local formspec = "size[12,12]" ..
				"label[0,0;Definition for level, " ..level.. "]" ..
				"field[1,1.5;3,1;name;Name;]" ..
				"image[10,2;2,2;"..fs_picture.."]"..
				"button[1,3;3,1;basepoint;New base]"..
				"label[5,3;"..fs_basepoint.."]"..
				"button[1,2;2,1;save;Save]"
	return formspec
end

minetest.register_on_player_receive_fields(function(player,formname, fields)
	if not formname:match("^(level_editor:main_gui_)") then
		return false
	end
	if fields.quit == "true" then
		return true
	end
	print("formname "..formname)
	local _,level = formname:match("(level_editor:main_gui_)(.+)")
	print("level : "..level)
	print(dump("fields for main gui "..dump(fields)))
	local name = player:get_player_name()
	local leveldata = level_editor.getleveldata(level)
	local base_def = {}
	base_def = table.copy(leveldata.base_def)

	base_def.name = base_def.name or level
	if fields.basepoint then
		--local basepoint = {x=3.67,y=78.23,z=34.55}
		local basepoint = player:get_pos()
		basepoint = table.copy(basepoint)
		basepoint = level_editor.round(basepoint)
		base_def.basepoint = base_def.basepoint or {}
		base_def.basepoint = table.copy(basepoint)
	end

	local level_dir = minetest.get_modpath("level_editor").."/levels/"	
	local settings = Settings(level_dir..base_def.name..".conf")
	if fields.save then
		settings:set("name",base_def.name)
		settings:set("basepoint",minetest.pos_to_string(base_def.basepoint):match("([^%(].+[^%)])"))
	--	Settings(level_dir..base_def.name..".conf"):set(startpoint,base_def.startpoint)
	--	Settings(level_dir..base_def.name..".conf"):set(endpoint,base_def.endpoint)
		local success = settings:write()
		minetest.log("action",name.." recorded new definition for level "..base_def.name)
	end
	
	leveldata.base_def = base_def
	level_editor.save()
	level_editor.gui(name,base_def.name)
	
	print(".conf : "..level_dir..base_def.name..".conf")
	return true
end)
	
function level_editor.gui(name,level)
	--local name = creator:get_player_name()
	local leveldata = level_editor.getleveldata(level)
	local levelname = leveldata.base_def.name or level.." name not found in def"
	minetest.chat_send_player(name,"defining level "..levelname)
	local formspec = mk_fs(name,level)
	minetest.show_formspec(name,"level_editor:main_gui_"..level, formspec)
end

minetest.register_chatcommand("level", {
	params = "level_name or nothing",
	privs = "level",
	description = "Envoyer un joueur (ou tous les joueurs *) directement à un niveau",
	func = function(sender,options)
		assert(type(options) == "string")
		level_editor.gui(sender,options)
	end
})
