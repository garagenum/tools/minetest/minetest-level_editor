-- All about checkpoints

local on_punch_checkpoint = nil
on_punch_checkpoint = function(pos, node, clicker)
	local keys=clicker:get_player_control()
	local name = clicker:get_player_name()
	if minetest.check_player_privs(name, {level=true}) and keys["aux1"]==true then
		return -- if sneaking, on_punch calls normal destruct function
	else
		level_editor.activate_checkpoint(name, pos, node)
		level_editor.go_checkpoint(name)
	end
end

local on_construct_checkpoint = nil
on_construct_checkpoint = function(pos)
	local meta = minetest.get_meta(pos)
	local formspec = level_editor.make_formspec("info_")
	meta:set_string("formspec",formspec)
--	meta:set_string("formspec",level_editor.message_formspec)
end
local on_receive_checkpoints_fields = nil
on_receive_checkpoints_fields = function(pos, formname, fields, placer)
	print("fields received : "..dump(fields))
	local name = placer:get_player_name()
	if not minetest.check_player_privs(name, {level=true}) then
		minetest.chat_send_player(name,"Tu n'as pas le droit de modifier le formulaire")
		--gngui.show_hud(name,name,"central","no","blink",0,2)
		return
	else
		for i,v in pairs(fields) do
			if i ~= "quit" and i ~= "clear" then
				minetest.chat_send_player(name,"Info : "..v)
				local node = minetest.get_node(pos)
				node.name = "level_editor:checkpoint_"..i
				print("new name : "..node.name)
				minetest.swap_node(pos,node)
			elseif i == "clear" then
				local node = minetest.get_node(pos)
				node.name = "level_editor:checkpoint"
				print("new name : "..node.name)
				minetest.swap_node(pos,node)
				minetest.chat_send_player(name,"No info for this checkpoint")
			end
		end
	end
end

local register_checkpoints_nodes = nil
register_checkpoints_nodes = function(prefix)
	local messages = level_editor.get_pic_list(prefix)
	for i,message in ipairs(messages) do
		local _,name = level_editor.register_image_as_node("checkpoint",message)
	end
end
register_checkpoints_nodes("info_")
level_editor.register_image_as_node("checkpoint")

local custom_checkpoint_nodes = nil
custom_checkpoint_nodes = function()
	for nodename,nodedef in pairs(minetest.registered_nodes) do
		if nodename:match("^(level_editor:checkpoint)") then
			minetest.override_item(nodename,{on_punch = on_punch_checkpoint})
			minetest.override_item(nodename,{on_construct = on_construct_checkpoint})
			minetest.override_item(nodename,{on_receive_fields = on_receive_checkpoints_fields})
			if nodename ~= "level_editor:checkpoint" then
				minetest.override_item(nodename,{groups = {oddly_breakable_by_hand=1, picture=1, not_in_craft_guide=1, not_in_creative_inventory=1} })
			end
		end
	end
end
custom_checkpoint_nodes()

	-- If, by accident, a player gets a checkpoint item, he can't place it
minetest.register_on_placenode(function(pos, newnode, placer, oldnode, itemstack, pointed_thing)
	if string.match(newnode.name,"^level_editor:checkpoint") then
		local plname = placer:get_player_name()
		if not minetest.check_player_privs(plname,{level=true}) then
			minetest.log("warning","player "..plname.." tried to place checkpoint node at "..minetest.pos_to_string(pos))
			minetest.set_node(pos,oldnode)--"default:air")
		else return
		end
	end
end)

function level_editor.activate_checkpoint(name,pos,node)
	local player = minetest.get_player_by_name(name)
	local pldata = level_editor.getplayerdata(name)
	local info = ""
	if node then 
		local _,info = node.name:match("^(level.*checkpoint_)(.+)$") 
		info = info or "no_info_at_all"
		print("info for checkpoint at "..minetest.pos_to_string(pos).." : "..info)
	end

	if info ~= "no_info_at_all" then info = info else info = "no_info" end
	table.insert(pldata.checkpoint, {pos = pos, info = info })
	
	minetest.log("action","checkpoint defined for user "..name.." at pos "..minetest.pos_to_string(pldata.checkpoint[#pldata.checkpoint].pos).." with info "..(info or "no_info"))
	level_editor.save()
end

function level_editor.go_checkpoint(name)
	local player = minetest.get_player_by_name(name)
	local pldata = level_editor.getplayerdata(name)
	local checkpoint = pldata.checkpoint[#pldata.checkpoint]
	player:setpos(checkpoint.pos)
	minetest.log("action",name.." respawned at "..minetest.pos_to_string(checkpoint.pos).." with info : "..checkpoint.info)
			--gngui.show_hud(plname,plname,"central","ok","blink",0,2)
	--minetest.after(0, function()
	--	level_editor.shownextform(name,pos,1)
	--	end)
end

	



