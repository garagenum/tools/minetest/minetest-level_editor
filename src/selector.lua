-- This file intends to create a formspec letting you choose one 
-- or several pictures from textures folder, given a pattern prefix

local pictures_dir = minetest.get_modpath("level_editor").."/textures/"
local pic_list = minetest.get_dir_list(pictures_dir,false)

level_editor.get_pic_list = function(prefix)
	local messages = {}
	local patterndef
	if prefix then
		patterndef = prefix..".+%.png" --or "^info_.+%.png"
	else patterndef = ".+%.png"
	end
	print("pattern "..patterndef)
	print("pictures dir : "..pictures_dir)
	print("pic list : "..dump(pic_list))
	for i,v in ipairs(pic_list) do
		if level_editor.check_file(pictures_dir..v) and v:match(patterndef) then
			local _,message,_= v:match("^("..prefix..")(.*)(%.png)$")
			print(" message : "..message)
			table.insert(messages,message)
		end
	end
	print("dump messages : "..dump(messages))
	return messages
end


local mk_formspec = nil
mk_formspec = function (messages)
	local fwidth = 15
	local fheight = 11
	local message_formspec = "size["..fwidth..","..fheight..",false]"..
		"no_prepend[]"..
		"label[2,0;Click on picture infos]"..
		"button_exit[13,0;2,1;clear;Clear]"
	local p,c = math.modf(math.sqrt(fheight))	
	for i,v in ipairs(messages) do
		local e,r = math.modf((i-1)/p)
		local xpos = e*p
	--	if i <= fheight-7 then
		local ypos = i-xpos
		ypos = ypos*2+(ypos-1) 				
		local length = #messages
		local f,s = math.modf((length-1)/p)
		xpos = xpos+(fwidth/2-(2*f-1)) 
		message_formspec = message_formspec.."image_button_exit["..xpos..","..ypos..";2,2;info_"..v..".png;"..v..";]"
	end
	return message_formspec
end

function level_editor.make_formspec(pattern)
	local messages = {}
	messages = level_editor.get_pic_list(pattern)
	formspec = mk_formspec(messages)
	print("dump formspec "..dump(formspec))
	return formspec
end

function level_editor.register_image_as_node(nodename,picture)
	assert(type(nodename)  ==  "string")
	local completename
	if picture then
		completename = nodename.."_"..picture
	else
		completename = nodename
	end
	local node = "level_editor:"..completename
	minetest.register_node(node, {
		description = completename,
		drawtype = "signlike",
		tiles = {nodename..".png"},
		visual_scale = 1.0,
		inventory_image = nodename..".png",
		wield_image = nodename..".png",
		paramtype = "light",
		paramtype2 = "wallmounted",
		sunlighmt_propagates = true,
		walkable = false,
		selection_box = {
			type = "wallmounted"
		},
		groups = {oddly_breakable_by_hand=1, picture=1, not_in_craft_guide=1},-- not_in_creative_inventory=1},
		can_dig = function(pos,digger)
			local plname = digger:get_player_name()
			local keys=digger:get_player_control()
			if not minetest.check_player_privs(plname,{level=true}) and keys["aux1"]==true then
				return false
			else return true
			end
		end,
	})
	return node
end				







--function level_editor.on_receive_fields(fields)
	
